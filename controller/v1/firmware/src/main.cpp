#include <Arduino.h>
#include "main.h"
#include "pages.h"
#include "ota.h"


#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <DNSServer.h>

#include <WiFiClient.h>
#include <WebSocketsServer.h>

#include <Servo.h>

// The prop also uses a servo-like signal but it needs a 54Hz rather than the default 50
// instead of using the servo library we call startWaveform from the esp core directly
// (looking at the Servo implementaion for this core reaveals that it's not complicated at all)
#include "core_esp8266_waveform.h"
#define PWM_PERDIOD 18400
#define PWM_MIN 1000
#define PWM_MAX 2000

#define AP_SSID "Hovercraft"
#define AP_PASS "HoverWiFi"
#define DNS_PORT 53

#define SERVO_PIN_RIGHT D1
#define SERVO_PIN_LEFT D2
#define PROP_PIN D3
#define DEBUG_PIN D7

DNSServer dnsServer;
ESP8266WebServer webServer(80);
WebSocketsServer webSocket(81);

Servo rightServo;
Servo leftServo;
Servo propeller;

int lastHeartbeat = 0;

bool armed = false;
bool started = false;

void handleControls(void);
void handleCmd(void);
void handleHeartbeat(void);
void handleNotFound(void);
void disarm(void);
void rearm(void);
void setSpeed(void);

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {

    switch(type) {
        case WStype_DISCONNECTED:
        {
            Serial.println("WS disconnected: disarming");
			lastHeartbeat = 0;
            disarm();
        }   break;
        case WStype_CONNECTED:
        {
            Serial.println("WS connected");
            rearm();
        }   break;
        case WStype_TEXT:
        {
			if (!started) {
				setSpeed();
			}
            if (payload[0] == 'H')
			{
				// Heartbeat
				lastHeartbeat = millis();
			}
			else if (payload[0] == 'T')
            {
                uint8_t val = (uint8_t) strtol((const char *) &payload[1], NULL, 0);
                int mapped = map(val, 0, 100, PWM_MIN, PWM_MAX);
                startWaveform(PROP_PIN, mapped, PWM_PERDIOD-mapped, 0);
            }
            else if (payload[0] == 'L')
            {
                uint8_t val = (uint8_t) strtol((const char *) &payload[1], NULL, 0);
                leftServo.write(val);
            }
            else if (payload[0] == 'R')
            {
                uint8_t val = (uint8_t) strtol((const char *) &payload[1], NULL, 0);
                rightServo.write(val);
            }
            else
            {
                Serial.printf("unrecognised payload: \"%s\"\n", payload);
            }
        }   break;
        case WStype_BIN:
            break;
    }
}

void setup ()
{
    Serial.begin(115200);
    Serial.println("Welcome to ESP8266 Hovercraft control.\n");

    setupOTA();
    Serial.println("OTA configured.\n");
    delay(500);
    
    uint32_t realSize = ESP.getFlashChipRealSize();
    uint32_t ideSize = ESP.getFlashChipSize();
    FlashMode_t ideMode = ESP.getFlashChipMode();
    
    Serial.printf("Flash real id:   %08X\n", ESP.getFlashChipId());
    Serial.printf("Flash real size: %u bytes\n", realSize);
    
    Serial.printf("Flash ide speed: %u Hz\n", ESP.getFlashChipSpeed());
    Serial.printf("Flash ide mode:  %s\n", (ideMode == FM_QIO ? "QIO" : ideMode == FM_QOUT ? "QOUT" : ideMode == FM_DIO ? "DIO" : ideMode == FM_DOUT ? "DOUT" : "UNKNOWN"));

    delay(500);
    rightServo.attach(SERVO_PIN_RIGHT);
    leftServo.attach(SERVO_PIN_LEFT);
    //propeller.attach(PROP_PIN);
    pinMode(PROP_PIN, OUTPUT);
    digitalWrite(PROP_PIN, LOW);
    pinMode(DEBUG_PIN, OUTPUT);
    digitalWrite(DEBUG_PIN, LOW);
    Serial.println("peripherals established.");

    delay(500);
    IPAddress local_IP(192,168,1,1);
    IPAddress gateway(192,168,1,1);
    IPAddress subnet(255,255,255,0);
    WiFi.mode(WIFI_AP);
    WiFi.softAPConfig(local_IP, gateway, subnet);
    
    if (!WiFi.softAP(AP_SSID, AP_PASS))
    {
        Serial.println("AP mode failed :s");
    }
    else
    {
        Serial.print("Access point up: ");
        Serial.println(WiFi.softAPIP());
    }
    
    
    dnsServer.setErrorReplyCode(DNSReplyCode::NoError);
    dnsServer.start(DNS_PORT, "*", WiFi.softAPIP());
    Serial.println("(fake) DNS server established.");

    webServer.on(String(F("/controls.html")), handleControls);
    webServer.on(String(F("/cmd")), handleCmd);
    webServer.on(String(F("/hb")), handleHeartbeat);
    webServer.onNotFound(handleNotFound);
    webServer.begin();
    Serial.println("webserver up and running.");

    webSocket.begin();
    webSocket.onEvent(webSocketEvent);
    Serial.println("websocket server started.");
    
    Serial.println("setup done");
}


void rearm()
{
    armed = true;
	started = false;
}

void setSpeed()
{
	started = true;
    digitalWrite(DEBUG_PIN, HIGH);
    startWaveform(PROP_PIN, PWM_MIN, PWM_PERDIOD - PWM_MIN, 0);
    digitalWrite(DEBUG_PIN, LOW);
}

void disarm()
{
    armed = false;
	started = false;
    stopWaveform(PROP_PIN);
    digitalWrite(PROP_PIN, LOW);
    leftServo.write(90);
    rightServo.write(90);
}

void loop ()
{
    ArduinoOTA.handle();
    webServer.handleClient();
    webSocket.loop();
    dnsServer.processNextRequest();


    bool hbLost = millis() - lastHeartbeat > 500;
    if (armed && started && hbLost)
    {
        // this should disarm the motor
        Serial.println("No heartbeat: disarming...");
        disarm();
    }
    else if (!armed && !hbLost)
    {
        Serial.println("heartbeat recovered");
        rearm();
    }
}


void handleControls ()
{
    Serial.println("serving controls.html");
    webServer.send(200, "text/html", FPSTR(controls_txt));
}


void handleCmd ()
{
    Serial.println("new values received:");
    Serial.println("arguments:");
    for (int i = 0; i < webServer.args(); i++)
    {
        Serial.print("Arg " + (String)i + " –> ");
        Serial.print(webServer.argName(i) + ": ");
        Serial.println(webServer.arg(i));
        String var = webServer.argName(i);
        int val = webServer.arg(i).toInt();
        if (var.equals("thrust"))
        {
            Serial.print("thrust: ");
            Serial.println(val);
            int mapped = map(val, 0, 100, PWM_MIN, PWM_MAX);
            startWaveform(PROP_PIN, mapped, PWM_PERDIOD-mapped, 0);
        }
        else if (var.equals("left"))
        {
            Serial.print("left: ");
            Serial.println(val);
            leftServo.write(val);
        }
        else if (var.equals("right"))
        {
            Serial.print("right: ");
            Serial.println(val);
            rightServo.write(val);
        }
    }
    webServer.send(200,"text/plain","ok");
}

void handleHeartbeat ()
{
    lastHeartbeat = millis();
    //Serial.println("heart beat received ...");
    webServer.send(200,"text/plain","ok");
}

void handleNotFound ()
{
    webServer.send(200, "text/html", FPSTR(portal_txt));
}
