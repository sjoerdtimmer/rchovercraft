#ifndef __ota_h__
#define __ota_h__

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

void setupOTA();

#endif
