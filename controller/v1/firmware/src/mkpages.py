import json

with open("pages.h", "w") as ofile:
    ofile.write("#ifndef __pages_h__\n")
    ofile.write("#define __pages_h__\n")
    ofile.write("#include <avr/pgmspace.h>\n")

    for pagename in ["portal", "controls"]:
        ofile.write("const char "+pagename+"_txt[] PROGMEM = ")
        with open(pagename+".html", "r") as ifile:
            for line in ifile:
                ofile.write("\n\t")
                ofile.write(json.dumps(line))
            ofile.write(";\n");
            
    ofile.write("#endif");


            
    
    
